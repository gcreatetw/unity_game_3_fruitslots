﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class CSGameManager : MonoBehaviour {

	public static CSGameManager instance = null;


    [Header("Links")]
    // Andoid App Link
    public string androidAppLink = "market://details?id=YOUR_ID";
    // App Store App Link
    public string iosAppLink = "itms-apps://itunes.apple.com/app/idYOUR_ID";

    [Header("Other")]
    //Support Email
    public string supportEmail = "";

	void Awake ()
	{
		if (instance == null)
		{
			DontDestroyOnLoad (gameObject);
			instance = this;
            Loaded();
		}
		else if (instance != this)
		{
			Destroy (gameObject);
		}
	}

    private void Loaded()
    {
        Application.targetFrameRate = 60;
    }

    // Rate app using app links
    public void Rate()
    {
#if UNITY_ANDROID
        Application.OpenURL(androidAppLink);
#elif UNITY_IPHONE
        Application.OpenURL(iosAppLink);
#endif
    }


}
