﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CSToggleGroupHelper : MonoBehaviour
{
    List<Toggle> source_list;

    private System.Reflection.FieldInfo _toggleListMember;
    ToggleGroup m_Group;

    void Start()
    {
        m_Group = this.GetComponent<ToggleGroup>();

        if (m_Group.allowSwitchOff)
        {
            return;
        }
        m_Group.allowSwitchOff = true;

        if (_toggleListMember == null)
        {
            _toggleListMember = typeof(ToggleGroup).GetField("m_Toggles", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            if (_toggleListMember == null)
                throw new System.Exception("UnityEngine.UI.ToggleGroup Has become public, so this code must be changed!");
        }
        source_list = _toggleListMember.GetValue(m_Group) as List<Toggle>;
        if (source_list != null)
        {
            foreach (Toggle tt in source_list)
            {
                tt.onValueChanged.AddListener((bool on) => { ToggleChanged(tt, on); });
            }
        }
    }

    private void ToggleChanged(Toggle tt, bool on)
    {
        if (!on)
        {
            if (!m_Group.AnyTogglesOn())
            {
                tt.isOn = true;
            }
        }
    }
}
